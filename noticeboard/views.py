from django.shortcuts import render
from .models import Notice

# Create your views here.
def notice_board(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        text = request.POST.get('text')
        contact_number = request.POST.get('contact_number')
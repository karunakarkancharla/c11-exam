from django.db import models



# Create your models here.

class Notice(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    contact_number = models.IntegerField()    

    def str(self):
        return f'Notice:{self.tile}:{self.text}:{self.contact_number}'

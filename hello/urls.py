from django.urls import path
from . views import say_hello
from . import views

urlpatterns = [
    path('sayhello', views.say_hello, name='say_hello'),
    path('saygoodbye', views.say_goodbye,name='say_goodbye'),
    path('fibonacci',views.get_nth_fibonacci,name='nth_fibonacci')

]
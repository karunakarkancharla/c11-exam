from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def say_hello(request):
    context = {'greeting':"Hi world!"}
    return render(request, 'hello/greetings.html',context)

def say_goodbye(request):
    context = {'greeting':"Goodbye"}
    return render(request, 'goodbye/greetings.html', context)

def nth_fibonacci(n):
    if n==0:
        return 0
    if n==1:
        return 1
    return nth_fibonacci(n-1) + nth_fibonacci (n-2)

def get_nth_fibonacci(request):
    n = int(request.Get.get('n', 0))
    fibonacci = nth_fibonacci(n)
    context = {'n':n,'fibonacci':fibonacci}
    return render(request, 'hello/fibonacci.html', context)

